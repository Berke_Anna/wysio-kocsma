package hu.wysion.kocsma.converter;

import hu.wysion.kocsma.dto.DrinkDto;
import hu.wysion.kocsma.entity.Drink;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.springframework.util.StringUtils.hasText;

@Component
public class DrinkConverter {

    public DrinkDto convertEntityToDto(Drink drink) throws DataCantBeNegative {
        DrinkDto drinkDto = new DrinkDto();
        drinkDto.setID(drink.getId());
        if (hasText(drink.getDrinkName())) {
            drinkDto.setDrinkName(drink.getDrinkName());
        }
        if (Objects.nonNull(drinkDto.getAlcoholPercent())) {
            if (drinkDto.getAlcoholPercent() > 0) {
                drinkDto.setAlcoholPercent(drink.getAlcoholPercent());
            } else {
                throw new DataCantBeNegative();
            }
        }
        if (Objects.nonNull(drinkDto.getDoseAmount())) {
            if (drinkDto.getDoseAmount() > 0) {
                drinkDto.setDoseAmount(drink.getDoseAmount());
            } else {
                throw new DataCantBeNegative();
            }
        }
        drinkDto.setConsumption(drink.getConsumption());
        return drinkDto;
    }

    public Drink convertDtoToEntity(DrinkDto drinkDto) throws DataCantBeNegative {
        Drink drink = new Drink();
        if (hasText(drinkDto.getDrinkName())) {
            drink.setDrinkName(
                    drinkDto.getDrinkName());
        }


        if (Objects.nonNull(drinkDto.getAlcoholPercent())) {
            if (drinkDto.getAlcoholPercent() <= 0) {
                throw new DataCantBeNegative();
            }
            drink.setAlcoholPercent(
                    drinkDto.getAlcoholPercent());

        }

        if (Objects.nonNull(drinkDto.getDoseAmount())) {
            if (drinkDto.getDoseAmount() <= 0) {
                throw new DataCantBeNegative();
            }
            drink.setDoseAmount(
                    drinkDto.getDoseAmount());

        }
        return drink;
    }

}
