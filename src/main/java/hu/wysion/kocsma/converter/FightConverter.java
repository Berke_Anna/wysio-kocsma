package hu.wysion.kocsma.converter;

import hu.wysion.kocsma.dto.FightDto;
import hu.wysion.kocsma.entity.Fight;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.DateException;
import org.springframework.stereotype.Component;

@Component
public class FightConverter {


    public FightDto convertEntityToDto(Fight fight) throws DateException {
        FightDto fightDto = new FightDto();
        fightDto.setID(fight.getId());
        if (fight.getTheFightStartingDate().isBefore(fight.getTheFightEndingDate())) {
            fightDto.setTheFightStartingDate(fight.getTheFightStartingDate());
            fightDto.setTheFightEndingDate(fight.getTheFightEndingDate());
        } else {
            throw new DateException();
        }
        fightDto.setWinnerGuest(fight.getWinnerGuest());
        fightDto.setFightingGuests(fight.getFightingGuests());
        return fightDto;
    }

    public Fight convertDtoToEntity(FightDto fightDto) throws DataCantBeNegative, DateException {
        Fight fight = new Fight();
        if (fightDto.getTheFightStartingDate().isBefore(fightDto.getTheFightEndingDate())) {
            fight.setTheFightEndingDate(
                    fightDto.getTheFightEndingDate());
            fight.setTheFightStartingDate(
                    fightDto.getTheFightStartingDate());
        } else {
            throw new DateException();
        }

        fight.setWinnerGuest(
                fightDto.getWinnerGuest()
        );

        return fight;
    }

    public Fight updateEntity(Fight fight, FightDto fightDto) {

        fight.setFightingGuests(fightDto.getFightingGuests());
        fight.setTheFightEndingDate(fightDto.getTheFightEndingDate());
        fight.setTheFightStartingDate(fightDto.getTheFightStartingDate());
        fight.setWinnerGuest(fightDto.getWinnerGuest());

        return fight;
    }
}
