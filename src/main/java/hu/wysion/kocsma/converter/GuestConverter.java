package hu.wysion.kocsma.converter;

import hu.wysion.kocsma.dto.GuestDto;
import hu.wysion.kocsma.entity.Guest;
import hu.wysion.kocsma.enums.Liverstrength;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.LiverStrengthException;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.springframework.util.StringUtils.hasText;

@Component
public class GuestConverter {

    public GuestDto convertEntityToDto(Guest guest) {
        GuestDto guestDto = new GuestDto();
        guestDto.setId(guest.getId());
        guestDto.setNickname(guest.getNickname());
        guestDto.setBicepSize(guest.getBicepSize());
        guestDto.setLiverStrength(guest.getLiverStrength().name());
        return guestDto;
    }

    public Guest convertDtoToEntity(GuestDto guestDto) throws LiverStrengthException, DataCantBeNegative {
        Guest guest = new Guest();
        if (hasText(guestDto.getNickname())) {
            guest.setNickname(
                    guestDto.getNickname());
        }

        if (hasText(guestDto.getLiverStrength())) {
            try {
                guest.setLiverStrength(
                        Liverstrength.valueOf(guestDto.getLiverStrength()));
            } catch (IllegalArgumentException e) {
                throw new LiverStrengthException();
            }
        }

        if (Objects.nonNull(guestDto.getBicepSize())) {
            if (guestDto.getBicepSize() <= 0) {
                throw new DataCantBeNegative();
            }
            guest.setBicepSize(
                    guestDto.getBicepSize());

        }
        return guest;
    }

}
