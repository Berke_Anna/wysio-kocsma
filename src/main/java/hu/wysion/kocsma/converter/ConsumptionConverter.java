package hu.wysion.kocsma.converter;

import hu.wysion.kocsma.dto.ConsumptionDto;
import hu.wysion.kocsma.entity.Consumption;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import org.springframework.stereotype.Component;

@Component
public class ConsumptionConverter {


    public ConsumptionDto convertEntityToDto(Consumption consumption) {
        ConsumptionDto consumptionDto = new ConsumptionDto();
        consumptionDto.setID(consumption.getId());
        consumptionDto.setDrink(consumption.getDrink());
        consumptionDto.setHowManyUnit(consumption.getHowManyUnit());
        consumptionDto.setPub(consumption.getPub());
        return consumptionDto;
    }

    public Consumption convertDtoToEntity(ConsumptionDto consumptionDto) throws DataCantBeNegative {
        Consumption consumption = new Consumption();

        if (consumption.getHowManyUnit() <= 0) {
            throw new DataCantBeNegative();
        }
        consumption.setHowManyUnit(
                consumptionDto.getHowManyUnit());


        return consumption;
    }
}
