package hu.wysion.kocsma.converter;

import hu.wysion.kocsma.dto.PubDto;
import hu.wysion.kocsma.entity.Pub;

public class PubConverter {


    public PubDto convertEntityToDto(Pub pub) {
        PubDto pubDto = new PubDto();
        pubDto.setID(pub.getId());
        pubDto.setGuestArrived(pub.getGuestArrived());
        pubDto.setGuestLeave(pub.getGuestLeave());
        //Todo: getGoToDetox?
//        pubDto.setGoToDetox(pub.getGoToDetox());
        pubDto.setGuest(pub.getGuest());
        pubDto.setConsumptionList(pub.getConsumptionList());
        return pubDto;
    }

    public Pub convertDtoToEntity(PubDto pubDto) {
        Pub pub = new Pub();
        if ((pubDto.getGuestArrived().isBefore(pubDto.getGuestLeave()))) {
            pub.setGuestLeave(
                    pubDto.getGuestLeave()
            );
            pub.setGuestArrived(
                    pubDto.getGuestArrived()
            );
        }

        return pub;
    }

}
