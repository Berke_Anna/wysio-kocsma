package hu.wysion.kocsma.controller;

import hu.wysion.kocsma.dto.ConsumptionDto;
import hu.wysion.kocsma.entity.Consumption;
import hu.wysion.kocsma.exceptions.ConsumptionNotFoundException;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.service.ConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(path = "consumption")
public class ConsumptionController {
    @Autowired
    private ConsumptionService consumptionService;


    @PostMapping("/saveConsumption")
    private ConsumptionDto saveConsumption(@RequestBody Consumption consumption) {
        return consumptionService.saveConsumption(consumption);
    }

    @GetMapping("/fetchConsumptions")
    public List<ConsumptionDto> fetchConsumptionList() {
        return consumptionService.fetchConsumptionList();
    }

    @GetMapping("/fetchConsumptionById/{id}")
    public ConsumptionDto fetchConsumptionById(@PathVariable("id") Long consumptionId) throws ConsumptionNotFoundException {

        return consumptionService.fetchConsumptionById(consumptionId);
    }


    @PutMapping("/updateConsumption/{id}")
    public ConsumptionDto updateConsumption(@RequestBody ConsumptionDto consumptionDto,
                                            @PathVariable("id") Long consumptionId) throws DataCantBeNegative {
        return consumptionService.updateConsumption(consumptionDto, consumptionId);
    }

    @DeleteMapping("/deleteConsumption/{id}")
    public String deleteConsumptionById(@PathVariable("id") Long consumptionId) {
        consumptionService.deleteConsumptionByID(consumptionId);

        return "Deleted successfully";
    }


}
