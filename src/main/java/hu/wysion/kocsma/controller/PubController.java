package hu.wysion.kocsma.controller;

import hu.wysion.kocsma.dto.PubDto;
import hu.wysion.kocsma.entity.Pub;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.GuestNotFoundException;
import hu.wysion.kocsma.exceptions.LiverStrengthException;
import hu.wysion.kocsma.exceptions.PubNotFoundException;
import hu.wysion.kocsma.service.PubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "pub")
public class PubController {

    @Autowired
    private PubService pubService;

    @PostMapping("/savePub")
    private PubDto savePub(@RequestBody Pub pub) {
        return pubService.savePub(pub);
    }


    @GetMapping("/fetchPubs")
    public List<PubDto> fetchPubList() {
        return pubService.fetchPubList();
    }

    @GetMapping("/fetchPubById/{id}")
    public PubDto fetchPubById(@PathVariable("id") Long pubId) throws GuestNotFoundException, DataCantBeNegative, LiverStrengthException, PubNotFoundException {
        return pubService.fetchPubById(pubId);
    }

    @PutMapping("/updatePub/{id}")
    public PubDto updatePub(@RequestBody PubDto pubDto,
                            @PathVariable("id") Long pudId) throws GuestNotFoundException, DataCantBeNegative {
        return pubService.updatePub(pubDto, pudId);
    }

    @DeleteMapping("/deletePub/{id}")
    public String deletePubById(@PathVariable("id") Long pubId) {
        pubService.deletePubByID(pubId);

        return "Deleted successfully";
    }


}

