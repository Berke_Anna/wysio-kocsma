package hu.wysion.kocsma.controller;

import hu.wysion.kocsma.dto.DrinkDto;
import hu.wysion.kocsma.entity.Drink;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.DrinkNotFoundException;
import hu.wysion.kocsma.service.DrinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;

@RestController
@RequestMapping(path = "drink")
public class DrinkController extends ResponseEntityExceptionHandler {
    @Autowired
    private DrinkService drinkService;


    @PostMapping("/saveDrink")
    private DrinkDto saveDrinkt(@RequestBody Drink drink) throws DataCantBeNegative {
        return drinkService.saveDrink(drink);
    }

    @GetMapping("/fetchDrinks")
    public List<DrinkDto> fetchDrinkList() throws DataCantBeNegative {
        return drinkService.fetchDrinkList();
    }

    @GetMapping("/fetchDrinkById/{id}")
    public DrinkDto fetchDrinkById(@PathVariable("id") Long drinkId) throws DrinkNotFoundException, DataCantBeNegative {
        return drinkService.fetchDrinkById(drinkId);
    }


    @PutMapping("/updateDrink/{id}")
    public DrinkDto updateDrink(@RequestBody DrinkDto drinkDto,
                                @PathVariable("id") Long drinkId) throws DrinkNotFoundException, DataCantBeNegative {
        return drinkService.updateDrink(drinkDto, drinkId);
    }

    @DeleteMapping("/deleteDrink/{id}")
    public String deleteDrinkById(@PathVariable("id") Long drinkId) {
        drinkService.deleteDrinkByID(drinkId);

        return "Deleted successfully";
    }

}
