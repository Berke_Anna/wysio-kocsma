package hu.wysion.kocsma.controller;

import hu.wysion.kocsma.dto.FightDto;
import hu.wysion.kocsma.entity.Fight;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.DateException;
import hu.wysion.kocsma.exceptions.FightNotFoundException;
import hu.wysion.kocsma.service.FightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "fight")
public class FightController {
    @Autowired
    private FightService fightService;

    @PostMapping("/saveFight")
    private FightDto saveFight(@RequestBody Fight fight) throws DateException {
        return fightService.saveFight(fight);
    }

    @GetMapping("/fetchFights")
    public List<FightDto> fetchFightList() throws DateException {
        return fightService.fetchFightList();
    }

    @GetMapping("/fetchFightById/{id}")
    public FightDto fetchFightById(@PathVariable("id") Long fightId) throws FightNotFoundException, DateException {
        return fightService.fetchFightById(fightId);
    }

    @PutMapping("/updateFight/{id}")
    public FightDto updateFight(@RequestBody FightDto fightDto,
                                @PathVariable("id") Long fightId) throws DataCantBeNegative, DateException, FightNotFoundException {
        return fightService.updateFight(fightDto, fightId);
    }

    @DeleteMapping("/deleteFight/{id}")
    public String deleteFightById(@PathVariable("id") Long fightId) {
        fightService.deleteFightByID(fightId);

        return "Deleted successfully";
    }
}
