package hu.wysion.kocsma.controller;

import hu.wysion.kocsma.dto.GuestDto;
import hu.wysion.kocsma.entity.Guest;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.GuestNotFoundException;
import hu.wysion.kocsma.exceptions.LiverStrengthException;
import hu.wysion.kocsma.service.GuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;


@RestController
@RequestMapping(path = "guest")
public class GuestController extends ResponseEntityExceptionHandler {
    @Autowired
    private GuestService guestService;


    @PostMapping("/saveGuest")
    private GuestDto saveGuest(@RequestBody Guest guest) {
        return guestService.saveGuest(guest);
    }

    //Todo: GUestDto, a adatbázisból törölni a rossz májerősségűeket
    @GetMapping("/fetchGuests")
    public List<GuestDto> fetchGuestList() {
        return guestService.fetchGuestList();
    }

    @GetMapping("/fetchGuestById/{id}")
    public GuestDto fetchGuestById(@PathVariable("id") Long guestId) throws GuestNotFoundException, DataCantBeNegative, LiverStrengthException {
        return guestService.fetchGuestById(guestId);
    }

    @PutMapping("/updateGuest/{id}")
    public GuestDto updateGuest(@RequestBody GuestDto guestDto,
                                @PathVariable("id") Long guestId) throws GuestNotFoundException, DataCantBeNegative, LiverStrengthException {
        return guestService.updateGuest(guestDto, guestId);
    }

    @DeleteMapping("/deleteGuest/{id}")
    public String deleteGuestById(@PathVariable("id") Long guestId) {
        guestService.deleteGuestByID(guestId);

        return "Deleted successfully";
    }
}
