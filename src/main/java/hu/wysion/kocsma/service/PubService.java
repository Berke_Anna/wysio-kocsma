package hu.wysion.kocsma.service;

import hu.wysion.kocsma.converter.PubConverter;
import hu.wysion.kocsma.dto.PubDto;
import hu.wysion.kocsma.entity.Pub;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.GuestNotFoundException;
import hu.wysion.kocsma.exceptions.LiverStrengthException;
import hu.wysion.kocsma.exceptions.PubNotFoundException;
import hu.wysion.kocsma.repository.PubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PubService {


    @Autowired
    private PubRepository pubRepository;

    //    @Autowired
    private PubConverter pubConverter;


    public PubDto savePub(Pub pub) {
        Pub pubDB = pubRepository.save(pub);
        return pubConverter.convertEntityToDto(pubDB);
    }

    //Todo fetchPubList dto
    public List<PubDto> fetchPubList() {
        List<Pub> pubDB = pubRepository.findAll();

        List<PubDto> pubDtoList = new ArrayList<>();

        for (Pub pub : pubDB) {
            pubDtoList.add(pubConverter.convertEntityToDto(pub));
        }
        return pubDtoList;
    }

    public PubDto fetchPubById(Long pubId) throws GuestNotFoundException, DataCantBeNegative, LiverStrengthException, PubNotFoundException {
        Optional<Pub> pubDB = pubRepository.findById(pubId);
        if (pubDB.isPresent()) {
            return pubConverter.convertEntityToDto(pubDB.get());
        } else {
            //Todo: PubNotFoundException
            throw new PubNotFoundException(pubId);
        }

    }

    public PubDto updatePub(PubDto pubDto, Long pubId) throws GuestNotFoundException, DataCantBeNegative {
        Pub pubDB = pubRepository.findById(pubId).get();
        Pub modifiedPub = pubConverter.convertDtoToEntity(pubDto);
        Pub savedPub = pubRepository.save(modifiedPub);
        PubDto result = pubConverter.convertEntityToDto(savedPub);
        return result;
    }


    public void deletePubByID(Long pubId) {
        pubRepository.deleteById(pubId);
    }

}
