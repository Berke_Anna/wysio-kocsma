package hu.wysion.kocsma.service;

import hu.wysion.kocsma.converter.FightConverter;
import hu.wysion.kocsma.dto.FightDto;
import hu.wysion.kocsma.entity.Fight;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.DateException;
import hu.wysion.kocsma.exceptions.FightNotFoundException;
import hu.wysion.kocsma.repository.FightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FightService {


    @Autowired
    private FightRepository fightRepository;

    @Autowired
    private FightConverter fightConverter;


    public FightDto saveFight(Fight fight) throws DateException {
        Fight fightDB = fightRepository.save(fight);
        return fightConverter.convertEntityToDto(fightDB);
    }

    public List<FightDto> fetchFightList() throws DateException {
        List<Fight> fightDB = fightRepository.findAll();

        List<FightDto> fightDtoList = new ArrayList<>();
        for (Fight fight : fightDB) {
            fightDtoList.add(fightConverter.convertEntityToDto(fight));
        }

        return fightDtoList;
    }

    public FightDto fetchFightById(Long fightId) throws FightNotFoundException, DateException {
        Optional<Fight> fightDB = fightRepository.findById(fightId);
        if (fightDB.isPresent()) {
            return fightConverter.convertEntityToDto(fightDB.get());
        } else {
            throw new FightNotFoundException(fightId);
        }

    }

    //Todo: updateEntity????
    public FightDto updateFight(FightDto fightDto, Long fightId) throws DataCantBeNegative, DateException, FightNotFoundException {
        Optional<Fight> fightDB = fightRepository.findById(fightId);
        if (fightDB.isPresent()) {
            Fight modifiedFight = fightConverter.updateEntity(fightDB.get(), fightDto);
            Fight savedFight = fightRepository.save(modifiedFight);
            FightDto result = fightConverter.convertEntityToDto(savedFight);
            return result;
        } else {
            throw new FightNotFoundException(fightId);
        }

    }


    public void deleteFightByID(Long fightId) {
        fightRepository.deleteById(fightId);
    }
}
