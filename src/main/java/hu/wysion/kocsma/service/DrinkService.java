package hu.wysion.kocsma.service;

import hu.wysion.kocsma.converter.DrinkConverter;
import hu.wysion.kocsma.dto.DrinkDto;
import hu.wysion.kocsma.entity.Drink;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.DrinkNotFoundException;
import hu.wysion.kocsma.repository.DrinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DrinkService {

    @Autowired
    private DrinkRepository drinkRepository;

    @Autowired
    private DrinkConverter drinkConverter;

    public DrinkDto saveDrink(Drink drink) throws DataCantBeNegative {
        Drink drinkDB = drinkRepository.save(drink);
        return drinkConverter.convertEntityToDto(drinkDB);
    }

    public List<DrinkDto> fetchDrinkList() throws DataCantBeNegative {
        List<Drink> drinkDB = drinkRepository.findAll();

        List<DrinkDto> drinkDtoList = new ArrayList<>();

        for (Drink drink : drinkDB) {
            drinkDtoList.add(drinkConverter.convertEntityToDto(drink));
        }
        return drinkDtoList;
    }

    //Todo fetchGuestList dto
    public DrinkDto fetchDrinkById(Long drinkId) throws DrinkNotFoundException, DataCantBeNegative {
        Optional<Drink> drinkDB = drinkRepository.findById(drinkId);
        if (drinkDB.isPresent()) {
            return drinkConverter.convertEntityToDto(drinkDB.get());
        } else {
            throw new DrinkNotFoundException(drinkId);
        }
    }

    public DrinkDto updateDrink(DrinkDto drinkDto, Long drinkId) throws DrinkNotFoundException, DataCantBeNegative {
        Drink drinkDB = drinkRepository.findById(drinkId).get();
        Drink modifiedDrink = drinkConverter.convertDtoToEntity(drinkDto);
        Drink savedDrink = drinkRepository.save(modifiedDrink);
        DrinkDto result = drinkConverter.convertEntityToDto(savedDrink);
        return result;
    }


    public void deleteDrinkByID(Long drinkId) {
        drinkRepository.deleteById(drinkId);
    }
}
