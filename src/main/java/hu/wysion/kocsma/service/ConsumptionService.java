package hu.wysion.kocsma.service;

import hu.wysion.kocsma.converter.ConsumptionConverter;
import hu.wysion.kocsma.dto.ConsumptionDto;
import hu.wysion.kocsma.entity.Consumption;
import hu.wysion.kocsma.exceptions.ConsumptionNotFoundException;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.repository.ConsumptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ConsumptionService {

    @Autowired
    private ConsumptionRepository consumptionRepository;

    @Autowired
    private ConsumptionConverter consumptionConverter;

    public ConsumptionDto saveConsumption(Consumption consumption) {
        Consumption consumptionDB = consumptionRepository.save(consumption);
        return consumptionConverter.convertEntityToDto(consumptionDB);
    }

    public List<ConsumptionDto> fetchConsumptionList() {
        List<Consumption> consumptionsDB = consumptionRepository.findAll();

        List<ConsumptionDto> consumptionDtoList = new ArrayList<>();

        for (Consumption consumption : consumptionsDB) {
            consumptionDtoList.add(consumptionConverter.convertEntityToDto(consumption));
        }
        return consumptionDtoList;
    }

    public ConsumptionDto fetchConsumptionById(Long consumptionId) throws ConsumptionNotFoundException {
        Optional<Consumption> consumptionDB = consumptionRepository.findById(consumptionId);
        if (consumptionDB.isPresent()) {
            return consumptionConverter.convertEntityToDto(consumptionDB.get());
        } else {
            throw new ConsumptionNotFoundException(consumptionId);
        }
    }

    public ConsumptionDto updateConsumption(ConsumptionDto consumptionDto, Long consumptionId) throws DataCantBeNegative {
        Consumption consumptionDB = consumptionRepository.findById(consumptionId).get();
        Consumption modifiedConsumption = consumptionConverter.convertDtoToEntity(consumptionDto);
        Consumption savedConsumption = consumptionRepository.save(modifiedConsumption);
        ConsumptionDto result = consumptionConverter.convertEntityToDto(savedConsumption);
        return result;
    }


    public void deleteConsumptionByID(Long consumptionId) {
        consumptionRepository.deleteById(consumptionId);
    }
}
