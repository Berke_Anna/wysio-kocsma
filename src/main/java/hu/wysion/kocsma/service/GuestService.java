package hu.wysion.kocsma.service;

import hu.wysion.kocsma.converter.FightConverter;
import hu.wysion.kocsma.converter.GuestConverter;
import hu.wysion.kocsma.dto.GuestDto;
import hu.wysion.kocsma.entity.Guest;
import hu.wysion.kocsma.exceptions.DataCantBeNegative;
import hu.wysion.kocsma.exceptions.GuestNotFoundException;
import hu.wysion.kocsma.exceptions.LiverStrengthException;
import hu.wysion.kocsma.repository.FightRepository;
import hu.wysion.kocsma.repository.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GuestService {


    @Autowired
    private GuestRepository guestRepository;

    @Autowired
    private FightRepository fightRepository;

    @Autowired
    private GuestConverter guestConverter;

    @Autowired
    private FightConverter fightConverter;


    public GuestDto saveGuest(Guest guest) {
        Guest guestDB = guestRepository.save(guest);
        return guestConverter.convertEntityToDto(guestDB);
    }

    //Todo fetchGuestList dto
    public List<GuestDto> fetchGuestList() {
        List<Guest> guestDB = guestRepository.findAll();

        List<GuestDto> guestDtoList = new ArrayList<>();
        for (Guest guest : guestDB) {
            guestDtoList.add(guestConverter.convertEntityToDto(guest));
        }

        return guestDtoList;
    }

    public GuestDto fetchGuestById(Long guestId) throws GuestNotFoundException, DataCantBeNegative, LiverStrengthException {
        Optional<Guest> guestDB = guestRepository.findById(guestId);
        if (guestDB.isPresent()) {
            return guestConverter.convertEntityToDto(guestDB.get());
        } else {
            throw new GuestNotFoundException(guestId);
        }

    }

    public GuestDto updateGuest(GuestDto guestDto, Long guestId) throws GuestNotFoundException, DataCantBeNegative, LiverStrengthException {
        Guest guestDB = guestRepository.findById(guestId).get();
        Guest modifiedGuest = guestConverter.convertDtoToEntity(guestDto);
        Guest savedGuest = guestRepository.save(modifiedGuest);
        GuestDto result = guestConverter.convertEntityToDto(savedGuest);
        return result;
    }

    public GuestDto fetchGuestWithFightId(Long guestId) throws GuestNotFoundException, DataCantBeNegative, LiverStrengthException {
        Optional<Guest> guestDB = guestRepository.findById(guestId);
        if (guestDB.isPresent()) {

            return guestConverter.convertEntityToDto(guestDB.get());
        } else {
            throw new GuestNotFoundException(guestId);
        }

    }


    public void deleteGuestByID(Long guestId) {
        guestRepository.deleteById(guestId);
    }


}
