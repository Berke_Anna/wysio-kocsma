package hu.wysion.kocsma.repository;

import hu.wysion.kocsma.entity.Consumption;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsumptionRepository extends JpaRepository<Consumption, Long> {
}
