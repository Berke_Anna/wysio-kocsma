package hu.wysion.kocsma.repository;

import hu.wysion.kocsma.entity.Pub;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PubRepository extends JpaRepository<Pub, Long> {
}
