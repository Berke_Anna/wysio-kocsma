package hu.wysion.kocsma.repository;


import hu.wysion.kocsma.entity.Fight;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FightRepository extends JpaRepository<Fight, Long> {
}
