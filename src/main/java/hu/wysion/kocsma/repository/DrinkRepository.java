package hu.wysion.kocsma.repository;

import hu.wysion.kocsma.entity.Drink;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrinkRepository extends JpaRepository<Drink, Long> {
}
