package hu.wysion.kocsma.entity;

import hu.wysion.kocsma.enums.Liverstrength;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
@Data


public class Guest extends AbstractId {

    @Column(nullable = false)
    private String nickname;
    @Column
    @Enumerated(EnumType.STRING)
    private Liverstrength liverStrength;
    @Column
    private Double bicepSize;
    @OneToMany(mappedBy = "guest")
    private List<Pub> pubList;
    @ManyToMany(mappedBy = "fightingGuests")
    private List<Fight> FightList;
}

