package hu.wysion.kocsma.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
@Data

public class Drink extends AbstractId {

    @Column
    private String drinkName;
    @Column
    private Double alcoholPercent;
    @Column
    private Double doseAmount;
    @OneToOne(mappedBy = "drink")
    private Consumption consumption;
}
