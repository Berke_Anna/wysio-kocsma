package hu.wysion.kocsma.enums;

public enum Liverstrength {
    BABA_MAJ("Baba máj"),
    TINI_MAJ("Tini máj"),
    EGYETEMISTA_MAJ("Egyetemista máj"),
    GYEMANT_MAJ("Gyémánt máj"),
    FANNI_MAJ("Fanni máj");

    private String action;
    
    // getter method
    public String getAction() {
        return this.action;
    }

    // enum constructor - cannot be public or protected
    Liverstrength(String action) {
        this.action = action;
    }
}


