package hu.wysion.kocsma.exceptions;

public class DateException extends Exception {

    @Override
    public String getMessage() {
        return "The first date can't be later than the second date";
    }
}
