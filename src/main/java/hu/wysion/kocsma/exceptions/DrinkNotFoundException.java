package hu.wysion.kocsma.exceptions;

public class DrinkNotFoundException extends Exception {
    long id;

    public DrinkNotFoundException(long id) {
        this.id = id;
    }


    @Override
    public String getMessage() {
        return "Drink with " + id + " id not found";
    }
}
