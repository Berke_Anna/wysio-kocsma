package hu.wysion.kocsma.exceptions;

public class FightNotFoundException extends Exception {
    long id;

    public FightNotFoundException(long id) {
        this.id = id;
    }


    @Override
    public String getMessage() {
        return "Fight with " + id + " id not found";
    }
}

