package hu.wysion.kocsma.exceptions;

public class PubNotFoundException extends Exception {
    long id;

    public PubNotFoundException(long id) {
        this.id = id;
    }


    @Override
    public String getMessage() {
        return "Pub with " + id + " id not found";

    }
}
