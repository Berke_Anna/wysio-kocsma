package hu.wysion.kocsma.exceptions;

public class LiverStrengthException extends Exception {
    public LiverStrengthException() {
        super();
    }

    @Override
    public String getMessage() {
        return "The liver strength isn't exists";
    }
}