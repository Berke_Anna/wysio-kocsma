package hu.wysion.kocsma.exceptions;

public class GuestNotFoundException extends Exception {
    long id;

    public GuestNotFoundException(long id) {
        this.id = id;
    }


    @Override
    public String getMessage() {
        return "Guest with " + id + " id not found";
    }
}
