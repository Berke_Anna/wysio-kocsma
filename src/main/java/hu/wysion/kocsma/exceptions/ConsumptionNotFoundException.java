package hu.wysion.kocsma.exceptions;

public class ConsumptionNotFoundException extends Exception {
    long id;

    public ConsumptionNotFoundException(long id) {
        this.id = id;
    }


    @Override
    public String getMessage() {
        return "Consumption with " + id + " id not found";
    }
}
