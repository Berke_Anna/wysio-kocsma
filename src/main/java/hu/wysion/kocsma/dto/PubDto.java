package hu.wysion.kocsma.dto;

import hu.wysion.kocsma.entity.Consumption;
import hu.wysion.kocsma.entity.Guest;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class PubDto {
    private Long ID;
    private LocalDateTime guestArrived;
    private LocalDateTime guestLeave;
    private boolean goToDetox;
    private Guest guest;
    private List<Consumption> consumptionList;
}
