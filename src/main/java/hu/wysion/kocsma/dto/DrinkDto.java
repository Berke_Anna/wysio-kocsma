package hu.wysion.kocsma.dto;

import hu.wysion.kocsma.entity.Consumption;
import lombok.Data;

@Data
public class DrinkDto {
    private Long ID;
    private String drinkName;
    private Double alcoholPercent;
    private Double doseAmount;
    private Consumption consumption;

}
