package hu.wysion.kocsma.dto;

import lombok.Data;

@Data
public class GuestDto {
    private Long id;
    private String nickname;
    private String liverStrength;
    private Double bicepSize;
}