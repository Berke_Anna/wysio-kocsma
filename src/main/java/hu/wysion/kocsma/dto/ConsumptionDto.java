package hu.wysion.kocsma.dto;

import hu.wysion.kocsma.entity.Drink;
import hu.wysion.kocsma.entity.Pub;
import lombok.Data;

@Data
public class ConsumptionDto {
    private Long ID;
    private Drink drink;
    private int howManyUnit;
    private Pub pub;
}
