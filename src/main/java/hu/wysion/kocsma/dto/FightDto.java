package hu.wysion.kocsma.dto;

import hu.wysion.kocsma.entity.Guest;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class FightDto {
    private Long ID;
    private LocalDateTime theFightStartingDate;
    private LocalDateTime theFightEndingDate;
    private Guest winnerGuest;
    private List<Guest> fightingGuests;

}
